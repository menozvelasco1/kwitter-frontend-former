
import {
    domain,
    handleJsonResponse,
    getInitStateFromStorage,
    asyncInitialState,
    asyncCases,
    createActions,
    createReducer
} from "../../../redux/helpers";

const url = domain + "/users";

const USERSINFO = createActions('usersinfo');

export const users = username=> (dispatch, getState) => {
    dispatch(USERSINFO.START());

    const username = getState().auth.login.result.userName;

    return fetch(url + "/" + username)
        .then(handleJsonResponse)
        .then(result => {
            dispatch(USERSINFO.SUCCESS(result))
        })
        .catch(err => Promise.reject(dispatch(USERSINFO.FAIL(err))));
};


export const UsersReducers = {
    
    usersinfo: createReducer(getInitStateFromStorage("usersinfo", asyncInitialState), {
        ...asyncCases(USERSINFO),
    }),
};
