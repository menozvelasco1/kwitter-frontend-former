import React, { Component } from "react";
import "./SignupForm.css";
import { connect } from 'react-redux';
import { users } from '../../redux/Registration/Registration';
import 'bootstrap/dist/css/bootstrap.min.css';

class SignupForm extends Component {

    state = {

        username: "",
        displayName: "",
        password: "",
    };

    

    handleChange = (e) => {

        this.setState({ [e.target.name]: e.target.value });


    };

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
        this.props.users(this.state)
    };

    render() {

        return (
            <>
            <div class="d-flex justify-content-center">
            <div class="text-center">
            <h1>Registration Form</h1>
                <form id="signupForm" >

                    <label htmlFor="username">Username</label>
                    <input
                        type="text"
                        name="username"
                        autoFocus
                        required
                        onChange={this.handleChange}
                    />
                    <label htmlFor="displayName">Displayname</label>
                    <input
                        type="text"
                        name="displayName"

                        required
                        onChange={this.handleChange}
                    />
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        name="password"
                        required
                        onChange={this.handleChange}
                    />
                    <button type="submit" onClick={this.handleSubmit} class="btn btn-primary">
                        Submit Form
                    </button>
                </form>
            </div>
            </div>
            </>
        )

    }
}

export default connect(null, { users })(SignupForm);
