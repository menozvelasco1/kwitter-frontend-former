import {
  domain,
  jsonHeaders,
  handleJsonResponse,
  asyncInitialState,
  asyncCases,
  createActions,
  createReducer
} from '../helpers';
import { getMessages } from './getMessages';

const url = domain + '/messages';

const NEWMESSAGES = createActions('newMessages');
export const newMessages = data => (dispatch, getState) => {
  dispatch(NEWMESSAGES.START());
  const token = getState().auth.login.result.token;
  dispatch(getMessages());
  return fetch(url, {
    method: 'POST',
    headers: { Authorization: 'Bearer ' + token, ...jsonHeaders },
    body: JSON.stringify({ text: data })
  })
    .then(handleJsonResponse)
    .then(result => dispatch(NEWMESSAGES.SUCCESS(result)))
    .catch(err => Promise.reject(dispatch(NEWMESSAGES.FAIL(err))));
};

export const newMessagesReducer = {
  newMessages: createReducer(asyncInitialState, {
    ...asyncCases(NEWMESSAGES)
  })
};
