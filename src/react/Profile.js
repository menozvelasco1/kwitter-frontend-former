import React, { Component } from 'react';
import { Menu } from './components';
import { userIsAuthenticated } from './HOCs';
import PasswordModal from './components/User/passwordModal';
import photo from '../image/photo.jpg';
import { connect } from 'react-redux';
import { domain } from '../redux/helpers';
import PhotoModal from './components/User/PhotoModal';
import UsersModal from './components/User/userModal';
import DeleteUser from './components/User/DeleteUser';
import {
  getuser,
  updateuser,
  // deleteuser,
  getuserlist
} from '../redux/Users/userinfo';
import 'bootstrap/dist/css/bootstrap.min.css';



class Profile extends Component {
  state = {
    passwordModal: false,
    photoModal: false,
    userModal: false
  };

  togglePassWord = () => {
    const newState = !this.state.passwordModal;
    this.setState({
      passwordModal: newState
    });
  };

  togglephoto = () => {
    const newState = !this.state.photoModal;
    this.setState({
      photoModal: newState
    });
  };
  toggleUser = () => {
    const newState = !this.state.userModal;
    this.setState({
      userModal: newState
    });
  };

  componentDidMount() {
    this.props.getuser(this.props.user.username);
  }

  render() {
    return (
      <>
        <Menu isAuthenticated={this.props.isAuthenticated} />
        <h2 class="text-center">Profile</h2>
        {this.props.details === null && (
          <img src={photo} alt="userphoto" style={{ width: '100px' }} />
        )}
        {this.props.details !== null && (
          <>
            {' '}

            <div class="text-center">     
              <div class="border">
            <div class="rounded-sm">

            <img             
              src={domain + this.props.details.user.pictureLocation}
              alt="userphoto"
              style={{ width: '100px' }}
              />
              </div>
              </div>   
              </div>
            <br />

            <div class="text-center">
            <h3>{this.props.details.user.username}</h3>
            <h3>{this.props.details.user.displayName}</h3>
            <h3>{this.props.details.user.about}</h3>
            </div>
          </>
        )}
        <div class="container">
          <div class="col-md-4">
            <div class="row">
              <button onClick={this.togglePassWord} type="button" class="btn btn-primary">change password</button>
              <button onClick={this.togglephoto} type="button" class="btn btn-primary">change photo</button>
              <button onClick={this.toggleUser} type="button" class="btn btn-primary">edit user</button>
              <DeleteUser />
            </div>
          </div>
        </div>

        <PasswordModal
          modal={this.state.passwordModal}
          toggle={this.togglePassWord}
        />
        <PhotoModal modal={this.state.photoModal} toggle={this.togglephoto} />
        <UsersModal modal={this.state.userModal} toggle={this.toggleUser} />
      </>
    );
  }
}

export default connect(
  state => ({
    user: state.auth.login.result,
    details: state.usersinfo.getuser.result
  }),

  {
    getuser, updateuser, getuserlist
  }
)(userIsAuthenticated(Profile));
