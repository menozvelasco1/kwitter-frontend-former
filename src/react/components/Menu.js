import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Menu.css';
import { connect } from 'react-redux';
import { logout } from '../../redux';
import { Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

class Menu extends Component {
  handleLogout = event => {
    event.preventDefault();
    this.props.logout();
    return <Redirect to="/" />;
  };

  render() {
    return (
      <div id="menu">
        <h1>Kwitter</h1>

        {this.props.isAuthenticated && (
          <div id="menu-links">
            <Link to="/mostlikes" type="button" class="btn btn-light">
              Top Ten Likes
            </Link>
            <Link to="/messagefeed" type="button" class="btn btn-light">
              Message Feed
            </Link>
            <Link to="/profiles/:username" type="button" class="btn btn-light">
              profile
            </Link>

            <Link
              to="/"
              onClick={this.handleLogout}
              type="button"
              class="btn btn-secondary"
            >
              Logout
            </Link>
          </div>
        )}
      </div>
    );
  }
}

export default connect(
  state => ({
    result: state.auth.logout.result,
    loading: state.auth.logout.loading,
    error: state.auth.logout.error
  }),
  { logout }
)(Menu);
