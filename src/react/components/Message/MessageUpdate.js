import React, { Component } from 'react';
import { newMessages } from '../../../redux/Messages/newMessages';
import { connect } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';

class MessageUpdate extends Component {
  state = { message: '' };

  handleMessage = e => {
    e.preventDefault();
    this.props.newMessages(this.state.message);
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    // const { loading, error } = this.props;
    return (
      <React.Fragment>
        <div class="col-ms-1">
        


        <form id="login-form" onSubmit={this.handleLogin}>
          <label htmlFor="message">message</label>
          <input
            type="message"
            name="message"
            required
            onChange={this.handleChange}
            />
        </form>
        <button onClick={this.handleMessage} type="submit" class="btn btn-primary">
          post message
        </button>
            </div>
            
        
        {/* {loading && <Spinner name="circle" color="blue" />}
        {error && <p style={{ color: 'red' }}>{error.message}</p>} */}
      </React.Fragment>
    );
  }
}

export default connect(
  null,
  { newMessages }
)(MessageUpdate);
