import React, { Component } from 'react';
import { Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';
import { connect } from 'react-redux';
import { changpassword } from '../../../redux/Users/changPassWord';

import { Redirect } from 'react-router-dom';

let newpassword = '';
class PassWordModal extends Component {
  state = {
    password: ''
  };

  handleChange = event => {
    console.log(event.target.value);
    this.setState({
      [event.target.name]: event.target.value
    });
    newpassword = event.target.value;
    console.log(newpassword);
    console.log(this.state);
  };

  changePassword = e => {
    e.preventDefault();

    this.props.changpassword(newpassword);

    return <Redirect to="/" />;
  };

  render() {
    return (
      <Modal
        centered
        size="lg"
        style={{ backgroundColor: 'grey' }}
        isOpen={this.props.modal}
      >
        <ModalHeader>change password</ModalHeader>
        <ModalBody>
          <form id="password-form">
            <label htmlFor="password">new password</label>
            <input
              type="text"
              name="password"
              autoFocus
              required
              onChange={this.handleChange}
            />
          </form>
        </ModalBody>
        <ModalFooter>
          <button onClick={this.changePassword}>submit</button>
          <button onClick={this.props.toggle}>cancel</button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default connect(
  null,
  { changpassword }
)(PassWordModal);
