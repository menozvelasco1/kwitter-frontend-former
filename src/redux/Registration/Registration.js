import { login } from "../auth"
import {
    domain,
    jsonHeaders,
    handleJsonResponse,
    getInitStateFromStorage,
    asyncInitialState,
    asyncCases,
    createActions,
    createReducer
} from "../helpers";

const url = domain + "/users";

const USERS = createActions("users");
export const users = usersData => dispatch => {
    dispatch(USERS.START());
    const loginData= {
        username: usersData.username,
        password: usersData.password,
    }

    return fetch(url , {
        method: "POST",
        headers: jsonHeaders,
        body: JSON.stringify(usersData)
    })
        .then(handleJsonResponse)
        .then(result => {
        dispatch(login(loginData))
        dispatch(USERS.SUCCESS(result))})
        .catch(err => Promise.reject(dispatch(USERS.FAIL(err))));
};


export const registrationReducers = {
    users: createReducer(getInitStateFromStorage("users", asyncInitialState), {
        ...asyncCases(USERS),
    }),
};
