import React, { Component } from "react";
import { LoginForm, Menu, } from "./components";
import { userIsNotAuthenticated } from "./HOCs";
import SignupForm from "./components/SignupForm";
import 'bootstrap/dist/css/bootstrap.min.css';

class Home extends Component {
  render() {
    return (
      <>
        <Menu />
        <h2 class="text-center">Your favorite microblogging platform</h2>
        <LoginForm />
        <br/>
        <SignupForm />
        
      </>
    );
  }
}

export default userIsNotAuthenticated(Home);
