import React, { Component } from 'react';
import Menu from './Menu';
import { userIsAuthenticated } from '../HOCs';
import { getMessages } from '../../redux/Messages/getMessages';
import { connect } from 'react-redux';
import MessageUpdate from './Message/MessageUpdate';
import { deleteMessages } from '../../redux/Messages/deleteMessage';
import { userlike } from '../../redux/Messages/likeUnlike';

class MostLikes extends Component {


    componentDidMount = () => {
        this.props.getMessages();
    }

    handledeleteMessages = (event, id) => {
        this.props.deleteMessages(id);
    };

    handleUserlike = (event, id) => {
        this.props.userlike(id);
    };


    render() {
        if (this.props.messages === null) {
            return (
                <React.Fragment key="11">
                    <Menu isAuthenticated={this.props.isAuthenticated} />
                    <MessageUpdate />

                    <h1> Message feed without Message</h1>
                </React.Fragment>
            );
        }
        let arr = this.props.messages.map(message => message)
        let sortLike = arr.sort((a, b) => b.likes.length - a.likes.length);
        console.log(sortLike.slice(0, 10))

        return (
            <>
                <Menu isAuthenticated={this.props.isAuthenticated} />
                <MessageUpdate />
                <h1>Top Ten Kwipps</h1>
                {sortLike.slice(0, 10).map(message => (
                    <React.Fragment key={message.id}>
                        <div key={message.id}>
                            <h6>author: {message.username}</h6>
                            <p>Text: {message.text}</p>
                            <p>Date: {message.createdAt}</p>
                            <p>like: {message.likes.length}</p>
                            {message.username === this.props.user.username && (
                                <button
                                    onClick={event =>
                                        this.handledeleteMessages(event, message.id)
                                    }
                                >
                                    Delete Message. mine
                                </button>
                            )}
                            <br />
                            <button onClick={event => this.handleUserlike(event, message.id)}>
                                like
                            </button >
                        </div>
                    </React.Fragment>
                ))}
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        messages: state.messages.getMessages.result,
        user: state.auth.login.result
    };
};

const mapDispatchToProp = {
    getMessages,
    deleteMessages,
    userlike
};

export default connect(
    mapStateToProps,
    mapDispatchToProp
)(userIsAuthenticated(MostLikes));
