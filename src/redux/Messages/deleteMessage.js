import {
  domain,
  jsonHeaders,
  handleJsonResponse,
  asyncInitialState,
  asyncCases,
  createActions,
  createReducer
} from '../helpers';
import { getMessages } from './getMessages';

const url = domain + `/messages`;

const DELETEMESSAGES = createActions('deleteMessages');
export const deleteMessages = id => (dispatch, getState) => {
  dispatch(DELETEMESSAGES.START());
  const token = getState().auth.login.result.token;
  return fetch(url + '/' + id, {
    method: 'DELETE',
    headers: { Authorization: 'Bearer ' + token, ...jsonHeaders }
  })
    .then(handleJsonResponse)
    .then(result => {
      dispatch(getMessages());
      dispatch(DELETEMESSAGES.SUCCESS(result));
    })
    .catch(err => Promise.reject(dispatch(DELETEMESSAGES.FAIL(err))));
};

export const deleteMessagesReducer = {
  deleteMessages: createReducer(asyncInitialState, {
    ...asyncCases(DELETEMESSAGES)
  })
};
