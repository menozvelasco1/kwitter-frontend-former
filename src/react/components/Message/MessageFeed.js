import React, { Component } from 'react';
import Menu from '../Menu';
import { userIsAuthenticated } from '../../HOCs';
import { getMessages } from '../../../redux/Messages/getMessages';
import { connect } from 'react-redux';
import MessageUpdate from './MessageUpdate';
import { deleteMessages } from '../../../redux/Messages/deleteMessage';
import { userlike } from '../../../redux/Messages/likeUnlike';
import 'bootstrap/dist/css/bootstrap.min.css';

class MessageFeed extends Component {
  componentDidMount() {
    this.props.getMessages();
    // console.log(this.props.messages);
  }

  handledeleteMessages = (event, id) => {
    this.props.deleteMessages(id);
  };

  handleUserlike = (event, id) => {
    this.props.userlike(id);
  };




  render() {
    if (this.props.messages === null) {
      return (
        <React.Fragment key="11">
          <Menu isAuthenticated={this.props.isAuthenticated} />
          <MessageUpdate />

          <h1> Message feed without Message</h1>
        </React.Fragment>
      );
    }

    return (
      <>
        <Menu isAuthenticated={this.props.isAuthenticated} />
        <MessageUpdate />
        <div class="text-center">
          <h1> Message Feed</h1>

        </div>
        {this.props.messages.map(message => (
          <React.Fragment key={message.id}>
            <div class="text-center">
              <div class="container">
              <div class="card">

                  <div class="border border-primary">
                    <div key={message.id}>
                
                      <h6 class="card-header text-white bg-primary">author: {message.username}</h6>
                
                      <p class="list-group-items">Text: {message.text}</p>
                      <p class="list-group-items">Date: {message.createdAt}</p>
                      <p class="list-group-items">like: {message.likes.length}</p>
              <button onClick={event => this.handleUserlike(event, message.id)} type="button" class="btn btn-primary">
                like
              </button>
                    </div>
                  </div>
                </div>
              </div>
              {message.username === this.props.user.username && (

                <div>
                  <button

                    onClick={event =>
                      this.handledeleteMessages(event, message.id)
                    }
                    type="button" class="btn btn-danger"
                  >
                    Delete Message. mine
                </button>
                </div>
              )}
               <br /> 
            </div>
          </React.Fragment>
        ))}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    messages: state.messages.getMessages.result,
    user: state.auth.login.result
  };
};

const mapDispatchToProp = {
  getMessages,
  deleteMessages,
  userlike
};

export default connect(
  mapStateToProps,
  mapDispatchToProp
)(userIsAuthenticated(MessageFeed));
