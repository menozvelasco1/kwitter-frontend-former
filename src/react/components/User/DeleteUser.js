import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deleteuser } from '../../../redux/Users/userinfo';
import { logout } from '../../../redux/auth';
// import { bindActionCreators } from "redux";

class DeleteUser extends Component {
  deleteuser = e => {
    const confirm = window.confirm('Do you want to delete?');
    if (confirm) {
      this.props.deleteuser();
    }
  };

  render() {
    // let button;
    // if (this.props.username === this.props.loggedIn) {

    //     button = <button onClick={this.deleteuser}>delete user</button>

    // }
    return (
      <React.Fragment>
        <button onClick={this.deleteuser} type="button" class="btn btn-danger">delete user</button>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    loggedIn: state.auth.login.result.username
  };
};
const mapDispatchToProp = {
  deleteuser,
  logout
};
export default connect(
  mapStateToProps,
  mapDispatchToProp
)(DeleteUser);
