import React, { Component } from 'react';
import { Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';
import { connect } from 'react-redux';
import { updateuser } from '../../../redux/Users/userinfo';

class UsersModal extends Component {
  state = {
    password: '',
    about: '',
    displayName: ''
  };

  handleChange = event => {
    console.log(event.target.value);
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleUpdate = e => {
    e.preventDefault();

    this.props.updateuser(this.state);
  };

  render() {
    return (
      <Modal
        centered
        size="lg"
        style={{ backgroundColor: 'grey' }}
        isOpen={this.props.modal}
      >
        <ModalHeader>edit user</ModalHeader>
        <ModalBody>
          <form id="password-form">
            <label htmlFor="password">password</label>
            <input
              type="text"
              name="password"
              autoFocus
              required
              onChange={this.handleChange}
            />
            <label htmlFor="displayName">displayName </label>
            <input
              type="text"
              name="displayName"
              autoFocus
              required
              onChange={this.handleChange}
            />
            <label htmlFor="about">about</label>
            <input
              type="text"
              name="about"
              autoFocus
              required
              onChange={this.handleChange}
            />
          </form>
        </ModalBody>
        <ModalFooter>
          <button onClick={this.handleUpdate}>submit</button>
          <button onClick={this.props.toggle}>cancel</button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default connect(
  null,
  { updateuser }
)(UsersModal);
