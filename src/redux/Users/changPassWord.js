import {
  domain,
  jsonHeaders,
  handleJsonResponse,
  getInitStateFromStorage,
  asyncInitialState,
  asyncCases,
  createActions,
  createReducer
} from '../helpers';
import { logout } from '../../redux/auth';

const url = domain;

const CHANGPASSWORD = createActions('changpassword');
export const changpassword = password => (dispatch, getState) => {
  dispatch(CHANGPASSWORD.START());
  const username = getState().auth.login.result.username;
  const token = getState().auth.login.result.token;
  dispatch(logout());

  return fetch(url + '/users/' + username, {
    method: 'PATCH',
    headers: { Authorization: 'Bearer ' + token, ...jsonHeaders },
    body: JSON.stringify({
      password: password
    })
  })
    .then(handleJsonResponse)
    .then(result => {
      console.log(result.statusCode);
      dispatch(CHANGPASSWORD.SUCCESS(result));
    })
    .catch(err => Promise.reject(dispatch(CHANGPASSWORD.FAIL(err))));
};

export const changePassWordReducers = {
  changpassword: createReducer(
    getInitStateFromStorage('changpassword', asyncInitialState),
    {
      ...asyncCases(CHANGPASSWORD)
    }
  )
};
