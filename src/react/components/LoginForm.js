import React, { Component } from 'react';
import Spinner from 'react-spinkit';
import { connect } from 'react-redux';
import { login } from '../../redux';
import 'bootstrap/dist/css/bootstrap.min.css';

import './LoginForm.css';

class LoginForm extends Component {
  state = { username: '', password: '' };

  handleLogin = e => {
    e.preventDefault();
    this.props.login(this.state);
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { loading, error } = this.props;
    return (
      <React.Fragment>
        {/* <div class="container"> */}

        <div class="d-flex justify-content-center">
      <div class="text-center">

        
        <form id="login-form" onSubmit={this.handleLogin}>
          <label htmlFor="username">Username</label>
          <input
            type="text"
            name="username"
            autoFocus
            required
            onChange={this.handleChange}
          />
          <label htmlFor="password">Password</label>
          <input
            type="password"
            name="password"
            required
            onChange={this.handleChange}
          />
          <button type="submit" disabled={loading} class="btn btn-primary btn-sm ">
            Login
          </button>
        </form>
        </div>
        </div>
      {/* </div> */}
        {loading && <Spinner name="circle" color="blue" />}
        {error && <p style={{ color: 'red' }}>{error.message}</p>}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    result: state.auth.login.result,
    loading: state.auth.login.loading,
    error: state.auth.login.error
  }),
  { login }
)(LoginForm);
