import React, { Component } from 'react';
import Menu from '../Menu';
import { userIsAuthenticated } from '../../HOCs';
import { getMessages } from '../../../redux/Messages/getMessages';
import { connect } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';

class MostLikes extends Component {
  componentDidMount() {
    this.props.getMessages();
    // console.log(this.props.messages);
  }

  render() {
    if (this.props.messages === null) {
      return (
        <React.Fragment key="61">
          <Menu isAuthenticated={this.props.isAuthenticated} />
          <h1> Message feed without Message</h1>
        </React.Fragment>
      );
    }
    let arr = this.props.messages.map(message => message);
    let sortLike = arr.sort((a, b) => b.likes.length - a.likes.length);
    for (let i = 0; i < 5; i++) {
      console.log(sortLike[i]);
    }
    return (
      <>
        <Menu isAuthenticated={this.props.isAuthenticated} />
        <div class="text-center">
          <h1>Ten Most Likes tweets</h1>
        </div>
        {sortLike.slice(0, 10).map(message => (
          <React.Fragment key={message.id}>
            <div class="text-center">
              <div class="container">
                <div class="card">
                  <div class="border border-primary">
                    <div key={message.id}>
                      <h6 class="card-header text-white bg-primary">
                        author: {message.username}
                      </h6>
                      <p class="list-group-items">Text: {message.text}</p>
                      <p class="list-group-items">Date: {message.createdAt}</p>
                      <div class="border border-primary text-primary">
                        <p class="list-group-items">
                          like: {message.likes.length}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <br />
            </div>
          </React.Fragment>
        ))}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    messages: state.messages.getMessages.result,
    user: state.auth.login.result
  };
};
const mapDispatchToProp = {
  getMessages
};
export default connect(
  mapStateToProps,
  mapDispatchToProp
)(userIsAuthenticated(MostLikes));
