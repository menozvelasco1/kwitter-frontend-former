import { createBrowserHistory } from 'history';
import { registrationReducers } from './Registration/Registration';
import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { reducers as authReducers } from './auth';
import { getMessagesReducer } from '../redux/Messages/getMessages';
import { newMessagesReducer } from '../redux/Messages/newMessages';
import { deleteMessagesReducer } from '../redux/Messages/deleteMessage';
import { userLikeReducers } from '../redux/Messages/likeUnlike';
import { changePassWordReducers } from '../redux/Users/changPassWord';
import { editPhotoReducer } from '../redux/Users/editPhoto';
import { reducers as UsersReducers } from '../redux/Users/userinfo';

export * from './auth';

export const history = createBrowserHistory({
  basename: process.env.PUBLIC_URL
});

export const store = configureStore({
  reducer: {
    router: connectRouter(history),
    auth: combineReducers(authReducers),
    messages: combineReducers(getMessagesReducer),
    registration: combineReducers(registrationReducers),
    newmessages: combineReducers(newMessagesReducer),
    deletemessage: combineReducers(deleteMessagesReducer),
    userlike: combineReducers(userLikeReducers),
    changepassword: combineReducers(changePassWordReducers),
    editphoto: combineReducers(editPhotoReducer),
    usersinfo: combineReducers(UsersReducers),
  },
  preloadedState: {},
  devTools: process.env.NODE_ENV !== 'production'
});

store.subscribe(() => {
  localStorage.setItem('login', JSON.stringify(store.getState().auth.login));
});
