import {
    domain,
    jsonHeaders,
    handleJsonResponse,
    getInitStateFromStorage,
    asyncInitialState,
    asyncCases,
    createActions,
    createReducer
  } from "../helpers";

  const url = domain + '/pictures';

  const USERPHOTOS = createActions('userPhotos');
  export const userPhotos = (photosInfo) => dispatch => {
      dispatch(USERPHOTOS.START());

      return fetch(url ,{
          method: 'PUT',
          header: jsonHeaders,
          body: JSON.stringify(photosInfo)
      })
      .then(handleJsonResponse)
      .then(result => {
          console.log(result);
          result = Object.keys(result.photos).map(key => result.photos[key]);
          dispatch(USERPHOTOS.SUCCESS(result));
      })

      .catch(err => {
        console.log(err)
        Promise.reject(dispatch(USERPHOTOS.FAIL(err)))
    });
};

export const userPhotosReducer = {
    userPhoto: createReducer(
        getInitStateFromStorage('userPhotos', asyncInitialState),
        {
            ...asyncCases(USERPHOTOS)
        }
    )
};