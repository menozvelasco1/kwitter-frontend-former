import {
  domain,
  jsonHeaders,
  handleJsonResponse,
  getInitStateFromStorage,
  asyncInitialState,
  asyncCases,
  createActions,
  createReducer
} from '../helpers';
import { getMessages } from './getMessages';

const url = domain;

const USERLIKE = createActions('userlike');
export const userlike = like => (dispatch, getState) => {
  dispatch(USERLIKE.START());
  const token = getState().auth.login.result.token;

  return fetch(url + '/likes', {
    method: 'POST',
    headers: { Authorization: 'Bearer ' + token, ...jsonHeaders },
    body: JSON.stringify({ messageId: like })
  })
    .then(handleJsonResponse)
    .then(result => {
      if (result.statusCode === 400) {
        dispatch(getMessages());
      } else if (result.statusCode === 400) {
        dispatch(userunlike(like));
      }
      dispatch(USERLIKE.SUCCESS(result));
    })
    .catch(err => {
      if (err.statusCode === 400) {
        dispatch(userunlike(like));
      }
      Promise.reject(dispatch(USERLIKE.FAIL(err)));
    });
};

const USERUNLIKE = createActions('userunlike');
export const userunlike = like => (dispatch, getState) => {
  dispatch(USERUNLIKE.START());
  const token = getState().auth.login.result.token;
  const username = getState().auth.login.result.username;

  return fetch(url + '/messages/' + like, {
    method: 'GET'
  })
    .then(handleJsonResponse)
    .then(result => {
      result.message.likes.map(each => {
        if (each.username === username) {
          const id = each.id;
          fetch(url + '/likes/' + id, {
            method: 'DELETE',
            headers: { Authorization: 'Bearer ' + token, ...jsonHeaders }
          })
            .then(handleJsonResponse)
            .then(result => {
              dispatch(getMessages());
              dispatch(USERUNLIKE.SUCCESS(result));
            });
        }
        return each.id;
      });
    });
};

export const userLikeReducers = {
  userlike: createReducer(
    getInitStateFromStorage('userlike', asyncInitialState),
    {
      ...asyncCases(USERLIKE)
    }
  ),
  userunlike: createReducer(
    getInitStateFromStorage('userunlike', asyncInitialState),
    {
      ...asyncCases(USERUNLIKE)
    }
  )
};
