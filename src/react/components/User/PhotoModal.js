import React, { Component } from 'react';
import { Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';
import { connect } from 'react-redux';
import { editPhoto } from '../../../redux/Users/editPhoto';

class PhotoModal extends Component {
  state = {
    photo: ''
  };

  handleChange = event => {
    console.log(event.target.value);
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  changePhoto = e => {
    const data = new FormData();
    data.append('picture', this.state.photo);
    this.props.editPhoto(e, data);
  };

  render() {
    return (
      <Modal
        centered
        size="lg"
        style={{ backgroundColor: 'grey' }}
        isOpen={this.props.modal}
      >
        <ModalHeader>photo</ModalHeader>
        <ModalBody>
          <form id="photo-form">
            <label htmlFor="photo">photo</label>
            <input
              type="file"
              name="photo"
              autoFocus
              required
              onChange={this.handleChange}
            />
          </form>
        </ModalBody>
        <ModalFooter>
          <button
            onClick={e => {
              this.props.toggle();
              this.changePhoto();
            }}
          >
            submit
          </button>
          <button onClick={this.props.toggle}>cancel</button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default connect(
  null,
  { editPhoto }
)(PhotoModal);
